#include <dlfcn.h>
#include <stdio.h>

void* realLib = 0;
void __tryInit()
{
	const char* realLibPath = "test/libtestlib.so";
	if(!realLib)
	{
		realLib = dlopen(realLibPath, RTLD_LAZY);
	}
}

void* __lookup(const char* f)
{
	__tryInit();
	return dlsym(realLib, f);
}

void* realReturn = 0;
void* _retEaxAtStart;
void _fakeReturn()
{
	void* eaxAtStart;
	asm("mov %%eax, %0\n\t" 
			: "=m"(eaxAtStart) : :);

	printf("User post function\n");
	asm __volatile__(
			"mov %1, %%eax\n\t"
			"mov %%ebp, %%esp\n\t"
			"pop %%ebp\n\t"
			"push %0\n\t"
			"ret"
			:
			: "m"(realReturn), "m"(eaxAtStart)
			:
			);
}



int doWork()
{
	void* eaxAtStart;
	void** espAtStart;
	asm("mov %%eax, %0\n\t" 
		"mov %%ebp, %1" 
			: "=m"(eaxAtStart), "=m"(espAtStart) : :);
	void* f = __lookup(__func__);

	void** retAddr = espAtStart + 1;
	realReturn = *retAddr;
	*retAddr = _fakeReturn;

	printf("User preamble\n");
	
	asm(
		"mov %%ebp, %%esp\n\t"
		"pop %%ebp\n\t"
		"mov %1, %%eax\n\t" 
		"jmp %0"
			:
			: "r"(f), "m"(eaxAtStart)
			: "%eax");
}
