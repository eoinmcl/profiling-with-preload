BINARY=testuser
SOURCES=main.cpp
OBJECTS=$(SOURCES:.cpp=.o)

CXX=g++
CXXFLAGS+=-I. -g
LDFLAGS=-Ltest -ltestlib -ldl

all: $(BINARY)

DEPFLAGS+=-I/usr/include/c++/4.4/ -I/usr/include/c++/4.4/i486-linux-gnu -I/usr/lib/gcc/i486-linux-gnu/4.4/include -I/usr/include/c++/4.4/backward/
depend:
		makedepend -- $(CXXFLAGS) $(DEPFLAGS) -- $(SOURCES)

$(BINARY): $(OBJECTS)
		$(CXX) $^ $(LDFLAGS) -o $@

clean:
		rm -rf $(OBJECTS) core $(BINARY)

# DO NOT DELETE
